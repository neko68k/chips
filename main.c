// CHIPS 84035 Performance Setting Tool V1.0
// (c)2019 Shaun "neko68k" Thompson

// details taken from CS4031 CHIPSet documentation
// the document is listed as 'preliminary' information
// but it seems like its pretty accurate


#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>

// write only, must be written for each DAT write
#define CONFIG_REG_PORT 0x22
// r/w but reads may provide unpredictable results according to documentation
#define CONFIG_DAT_PORT 0x23

#define REG_PERF_CTRL   8
#define REG_MISC_CTRL   9

#define write_reg(a,b)  outp(CONFIG_REG_PORT, a);outp(CONFIG_DAT_PORT, b);
#define read_reg(a,b)   outp(CONFIG_REG_PORT, a);b=inp(CONFIG_DAT_PORT);

#define TURBO_ON_MASK   0xFE
#define TURBO_OFF_MASK  0x01
#define CACHE_ON_MASK   0x80
#define CACHE_OFF_MASK  0x7F

const char* heading = "CHIPS 84035 Performance Setting Tool V1.0\n(c)2019 Shaun \"neko68k\" Thompson\n\n";
const char* usage = " -t = Turbo on\n -o = Turbo off\nTurbo must be off for the following options to take effect\n -c = Cache on\n -d = Cache off\n -g = Get current performance setting\n -wN = Set performance. Higher numbers are slower.  (N=1~127)\n";
const char* off = "OFF";
const char* on = "ON";

unsigned char get_perf();
void set_perf(unsigned char perf);

void turbo_on()
{
    unsigned char misc;

    read_reg(REG_MISC_CTRL, misc)
    misc &= TURBO_ON_MASK;
    write_reg(REG_MISC_CTRL, misc)
}

void turbo_off()
{
    unsigned char misc;
    
    // need to check the performance setting here
    // if its 0 the machine will hang immediately
    // so we should at least set it to 1 before disabling
    // turbo

    if(!(get_perf()&CACHE_OFF_MASK))
        set_perf(1);

    read_reg(REG_MISC_CTRL, misc)
    misc |= TURBO_OFF_MASK;
    write_reg(REG_MISC_CTRL, misc)
}

void cache_off()
{
    unsigned char misc;

    read_reg(REG_PERF_CTRL, misc)
    misc |= CACHE_ON_MASK;
    write_reg(REG_PERF_CTRL, misc)
}

void cache_on()
{
    unsigned char misc;

    read_reg(REG_PERF_CTRL, misc)
    misc &= CACHE_OFF_MASK;
    write_reg(REG_PERF_CTRL, misc)
}


unsigned char get_perf()
{
    // get perf control data
    unsigned char misc;

    read_reg(REG_PERF_CTRL, misc)
    return misc;
}

void set_perf(unsigned char perf)
{
    unsigned char misc;
    
    // clamp because 0 crashes and >127 disables cache
    // ad we do that somewhere else
    if(perf<1)
        perf=1;
    if(perf>127)
        perf=127;
    
    read_reg(REG_PERF_CTRL, misc)
    misc &= CACHE_ON_MASK;          // preserve cache flag
    misc |= perf;

    // set perf control data
    write_reg(REG_PERF_CTRL, misc);
}

void disp_current()
{
    unsigned char cur = 0;

    read_reg(REG_MISC_CTRL, cur)
    printf("Turbo: ");
    if(cur&TURBO_OFF_MASK)
        printf("%s\n", off);
    else
        printf("%s\n", on);

    printf("Cache: ");
    cur = get_perf();
    if(cur&CACHE_ON_MASK)
        printf("%s\n", off);
    else
    printf("%s\n", on);

    printf("Performance bits: %i\n", cur&CACHE_OFF_MASK);
}

int main(int argc, char* argv[])
{
    int opt;
    int disp = 1;

    printf(heading);

    while((opt = getopt(argc, argv, "htocdsw:")) !=-1)
    {
        switch(opt)
        {
        case 'h':
            printf(usage);
            break;
        case 't':
            // turbo on
            turbo_on();
            break;
        case 'o':
            // turbo off
            turbo_off();
            break;
        case 'c':
            // cache on
            cache_on();
            break;
        case 'd':
            // cache off
            cache_off();
            break;
        case 's':
            // silent mode
            disp = 0;
            break;
        case 'w':
            // set performance
            set_perf((unsigned char)atoi(optarg)&CACHE_OFF_MASK);
            break;
        default:
            printf(usage);
            exit(-1);
        }
    }

    if(disp)
        disp_current();

    return 0;
}

